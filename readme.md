# The 'List Of Things You Can Not Do' Software License
The first and only software license (saying otherwise violates the license)\
Inspired by [An Anti-License Manifesto](https://www.boringcactus.com/2021/09/29/anti-license-manifesto.html)\
See [the license file](LICENSE) for this license\
Everyone may (and must, or they're breaking the license) use this license for their software projects
